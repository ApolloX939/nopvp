
local interval = 0
minetest.register_globalstep(function(dtime)
    interval = interval + dtime
    if interval >= nopvp.settings.step_speed then
        for _, player in ipairs(minetest.get_connected_players()) do
            local name = player:get_player_name()
            local pos = player:get_pos()
            local hit = minetest.find_node_near(pos, nopvp.settings.radius, {"nopvp:node"})
            if hit then
                local rc = nopvp.enable_for(name)
                if rc.value then
                    minetest.chat_send_player(name, "PVP: Disabled")
                end
            else
                local rc = nopvp.disable_for(name)
                if rc.value then
                    minetest.chat_send_player(name, "PVP: Enabled")
                end
            end
        end
        interval = 0
    end
end)
