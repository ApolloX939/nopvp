
if _nopvp.cache.players == nil then
    if nopvp.settings.debug_logging then
        _nopvp.log("WARN Initialized cache.players list")
     end
    _nopvp.cache.players = {}
end

nopvp.enable_for = function(playername)
    local player = minetest.get_player_by_name(playername) or nil
    if not player then
        if nopvp.settings.debug_logging then
           _nopvp.log("ERR enable_for('" .. tostring(playername) .. "') > Player not online")
        end
        return {success=false, error="Player not online", value=nil}
    end
    for i, name in ipairs(_nopvp.cache.players) do
        if name == playername then
            if nopvp.settings.debug_logging then
                --_nopvp.log("WARN enable_for('" .. tostring(playername) .. "') > Player already enabled")
             end
            return {success=true, error=nil, value=false}
        end
    end
    if nopvp.settings.debug_logging then
        _nopvp.log("WARN enable_for('" .. tostring(playername) .. "') > Player now enabled")
    end
    table.insert(_nopvp.cache.players, playername)
    return {success=true, error=nil, value=true}
end

nopvp.disable_for = function (playername)
    local player = minetest.get_player_by_name(playername) or nil
    if not player then
        if nopvp.settings.debug_logging then
            _nopvp.log("ERR disable_for('" .. tostring(playername) .. "') > Player not online")
         end
        return {success=false, error="Player not online", value=nil}
    end
    if #_nopvp.cache.players == 0 then
        return {success=true, error=nil, value=false}
    end
    local idx = -1
    for i, name in ipairs(_nopvp.cache.players) do
        if name == playername then
            idx = i
        end
    end
    if idx ~= -1 then
        if nopvp.settings.debug_logging then
            _nopvp.log("WARN disable_for('" .. tostring(playername) .. "') > Player now disabled")
         end
        table.remove(_nopvp.cache.players, idx)
        return {success=true, error=nil, value=true}
    end
    if nopvp.settings.debug_logging then
        --_nopvp.log("WARN disable_for('" .. tostring(playername) .. "') > Player already disabled")
     end
    return {success=true, error=nil, value=false}
end

nopvp.disable_for_all = function ()
    if #_nopvp.cache.players == 0 then
        return
    end
    _nopvp.cache.players = {}
end
