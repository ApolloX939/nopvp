
minetest.register_node("nopvp:node", {
    short_description = "No PVP",
    description = "No PVP\nPrevents PVP within a " .. tostring(nopvp.settings.radius) .. " range\nBlocks " .. tostring(math.floor(nopvp.settings.damage_reduction * 100)) .. "% damage",
    tiles = {"nopvp_node.png"},
    groups = {crumbly = 3},
    sounds = default.node_sound_metal_defaults()
})
