
nopvp = {
    version = "1.0-rel",
    settings = {
        radius = 8, -- Distance nodes will check for players in
        damage_reduction = 1.0, -- blocks/reduces % of damage (1.0 == 100%, 0.5 == 50%, 0.0 == 0%)
        debug_logging = false, -- Dump enable_for and disable_for into logs
        step_speed = 1.0, -- Frequency at which the globalstep will update cache
    }
}

_nopvp = {
    cache = {}, -- In-memory cache
    log = function (msg)
        if type(msg) == "table" then
            msg = minetest.serialize(msg)
        end
        minetest.log("action", "[nopvp] " .. tostring(msg))
    end,
    dofile = function (file)
        local modpath = minetest.get_modpath("nopvp")
        dofile(modpath .. DIR_DELIM .. file .. ".lua")
    end
}

_nopvp.dofile("settings")
_nopvp.dofile("api")
_nopvp.dofile("node")
_nopvp.dofile("register")
_nopvp.dofile("globalstep")

_nopvp.log("===== ===== =====")
_nopvp.log("     No PVP")
_nopvp.log(" Version: " .. nopvp.version)
_nopvp.log(" " .. _VERSION)
_nopvp.log("===== ===== =====")
_nopvp.log("    Settings")
_nopvp.log("nopvp_radius = '" .. tostring(nopvp.settings.radius) .. "'")
_nopvp.log("nopvp_damage_reduction = '" .. tostring(nopvp.settings.damage_reduction) .. "'")
_nopvp.log("nopvp_debug_logging = '" .. tostring(nopvp.settings.debug_logging) .. "'")
_nopvp.log("nopvp_step_speed = '" .. tostring(nopvp.settings.step_speed) .. "'")
_nopvp.log("===== ===== =====")
