
local new = false

local radius = minetest.settings:get("nopvp_radius") or nil
if radius == nil then
    minetest.settings:set("nopvp_radius", "8")
    nopvp.settings.radius = 8
    new = true
else
    nopvp.settings.radius = math.floor(tonumber(radius))
    if nopvp.settings.radius > 16 then
        nopvp.settings.radius = 16
        minetest.settings:set("nopvp_radius", "16")
        new = true
    end
    if nopvp.settings.radius < 1 then
        nopvp.settings.radius = 1
        minetest.settings:set("nopvp_radius", "1")
        new = true
    end
end

local dmg_reduct = minetest.settings:get("nopvp_damage_reduction") or nil
if dmg_reduct == nil then
    minetest.settings:set("nopvp_damage_reduction", "1.0")
    nopvp.settings.damage_reduction = 1.0
    new = true
else
    nopvp.settings.damage_reduction = tonumber(dmg_reduct)
    if nopvp.settings.damage_reduction > 1.0 then
        nopvp.settings.damage_reduction = 1.0
        minetest.settings:set("nopvp_damage_reduction", "1.0")
        new = true
    end
    if nopvp.settings.damage_reduction < 0.0 then
        nopvp.settings.damage_reduction = 0.0
        minetest.settings:set("nopvp_damage_reduction", "0.0")
        new = true
    end
end

local debug = minetest.settings:get_bool("nopvp_debug_logging")
if debug == nil then
    minetest.settings:set("nopvp_debug_logging", "false")
    nopvp.settings.debug_logging = false
    new = true
else
   nopvp.settings.debug_logging = debug
end

local speed = minetest.settings:get("nopvp_step_speed") or nil
if speed == nil then
    minetest.settings:set("nopvp_step_speed", "1.0")
    nopvp.settings.step_speed = 1.0
    minetest.settings:set("nopvp_step_speed", "1.0")
    new = true
else
    nopvp.settings.step_speed = tonumber(speed)
    if nopvp.settings.step_speed > 60.0 then
        nopvp.settings.step_speed = 60.0
        minetest.settings:set("nopvp_step_speed", "60.0")
        new = true
    end
    if nopvp.settings.step_speed < 0.0 then
        nopvp.settings.step_speed = 0.0
        minetest.settings:set("nopvp_step_speed", "0.0")
        new = true
    end
end

if new then
    minetest.settings:write()
end
