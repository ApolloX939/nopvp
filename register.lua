
minetest.register_on_punchplayer(function(player, hitter, time_from_last_punch, tool_capabilities, dir, damage)
    if damage == 0 or damage < 0 then -- Ignore 0 or negative damage (negative damage could be considered healing effects)
        return
    end
	if hitter and hitter:is_player() and hitter:get_player_name()~=player:get_player_name() then
		for i, name in ipairs(_nopvp.cache.players) do
            if name == player:get_player_name() then
                local dmg = math.floor((damage * (1.0 - nopvp.settings.damage_reduction)))
                if nopvp.settings.debug_logging then
                    _nopvp.log("WARN minetest.register_on_punchplayer('" .. player:get_player_name() .. "', '" .. hitter:get_player_name() .. "') > Preventing " .. tostring(damage) .. " damage (" .. tostring(dmg) .. ")")
                end
                player:set_hp(player:get_hp()-dmg, "set_hp")
                return true
            end
        end
	end
end)
