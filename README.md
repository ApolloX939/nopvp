# No PVP

\[[Gitlab](https://gitlab.com/ApolloX939/nopvp)] \[[ContentDB]()]

v1.0-rel

## Settings

- `nopvp_radius` (integer, 1 to 16, default 8) Distance from a `nopvp:node` a player will be marked as unable to be fought
- `nopvp_damage_reduction` (float, 0.0 to 1.0, default 1.0) Percentage of damage reduced while a player is marked by a `nopvp:node` \*
- `nopvp_debug_logging` (bool, true or false, default false) Emit more log messages/events
- `nopvp_step_speed` (float, 0.0 to 60.0, default 1.0) Seconds till globalstep marks/unmarks players fighting status

> \* All `nopvp:node`'s can instead of complete 100% damage reduction, just reduce it. (By default however it blocks all 100% of it)
